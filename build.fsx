#r @"packages/FAKE/tools/FakeLib"
open Fake
open Fake.Testing

RestorePackages()

let buildDir = "./build"
let testDir  = "./test"

Target "Clean" <| fun _ -> 
  CleanDir buildDir
  CleanDir testDir

Target "BuildApp" <| fun _ ->
  !! "src/app/**/*.*fsproj"
    |> MSBuildRelease buildDir "Build"
    |> Log "AppBuild-Output: "

Target "BuildTest" <| fun _ -> 
  !! "src/test/**/*.?sproj"
    |> MSBuildDebug testDir "Build"
    |> Log "TestBuild-Output: "

Target "Test" <| fun _ ->
  !! (testDir + "*/*.test.dll")
    |> xUnit2 (fun p -> { p with HtmlOutputPath = Some(testDir @@ "xunit.html") })

Target "Default" <| fun _ ->
  trace "Hello World from FAKE"

"Clean"
  ==> "BuildApp"
  ==> "BuildTest"
  ==> "Test"
  ==> "Default"

RunTargetOrDefault "Default"
